

import java.io.IOException;
import java.util.Observable;


public class Joueur extends Observable implements IJoueur {
	

	public Joueur() {
		super();
		setMaFlotte(null);
	}
	public Joueur(Flotte maFlotte) {
		super();
		setMaFlotte(maFlotte);
	}
	public Joueur(String nom, Flotte maFlotte) {
		super();
		this.nom = nom;
		setMaFlotte(maFlotte);
	}
	
	
	private int x;
	private int y;
	private int etat;
	private String nom;
	
	
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.setNom(nom);
		setChanged();
		notifyObservers();
	}
	private Flotte maFlotte;
	
	public Flotte getMaFlotte() {
		return maFlotte;
	}
	public void setMaFlotte(Flotte maFlotte) {
		this.maFlotte = maFlotte;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() { 
		return y;
	}
	/**
	 * @param y Colonne
	 * 
	 */
	public void setY(int y) {
		this.y = y;
	}

	public int hit(){
		int ret = 0;
		try {
			ret = System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}
	public boolean tir(int x, int y, Flotte f) {
		if(f!=null){
			return f.isBatimentTouche(x, y);
		}
		return false;
	}
}
