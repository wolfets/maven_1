

import java.util.ArrayList;
import java.util.List;

public class Flotte implements IFlotte {

	public int getNbBatiment() {
		if (batiments==null) return  0;
		return batiments.size();
	}

	private List<Batiment> batiments;

	public Flotte() {

		batiments = new ArrayList<Batiment>();

		// ------
		Batiment b = new Batiment();
		// b.
	}

	public List<Batiment> getBatiments() {
		return batiments;
	}

	public void setBatiments(List<Batiment> batiments) {
		batiments = batiments;
	}
	public Batiment getBatiment(int x, int y) {
		if(x==10 && y==10){
			x+=0;
		}
		for (Batiment bt : batiments) {
			for (Bloc b : bt.getBlocs()) {
				if(b.getX()==x && b.getY()==y)
					return bt;
			}
		}
		return null;
	}

	public Flotte createBatiment(String c, int x, int y, int l) {
		if(batiments==null){
			batiments = new ArrayList<Batiment>();
		}
		Batiment b = new Batiment(c, x, y, l);
		batiments.add(b);
		return this;
	}
	/**
	 * @return
	 */
	public Batiment addBatiment(Batiment b) {
		batiments.add(b);
		return b;

	}

	public boolean isBatimentTouche(int x, int y) {
		boolean r = false;
		for (Batiment b : batiments) {
			if (!r) {
				r = b.isBlocTouche(x, y);
			}
		}

		return r;
	}

	public boolean isFlotteCoule() {
		int totBlocValid = 0;
		for (Batiment b : batiments) {
			totBlocValid += (b.nbBlocEncoreValid());
		}

		return (totBlocValid==0);
	}

	public int getReste() {
		int totBlocValid = 0;
		for (Batiment b : batiments) {
			totBlocValid += (b.nbBlocEncoreValid());
		}

		return totBlocValid;
	}
	
}
