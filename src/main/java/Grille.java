

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.BoxLayout;

import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class Grille extends JPanel implements IGrille {

	private JPanel contentPane;

	private static Flotte flotte;
	private static Joueur joueur;

	private String Dir = "V";
	
	
	public String getDir() {
		return Dir;
	}
	public void setDir(String dir) {
		Dir = dir;
	}
	
	
	public Flotte getFlotte() {
		return flotte;
	}

	public void setFlotte(Flotte flotte) {
		this.flotte = flotte;
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					flotte = new Flotte();
					joueur = new Joueur();

					flotte.createBatiment("V", 1, 2, 3);
					flotte.createBatiment("H", 5, 3, 2);

					Grille frame = new Grille(flotte);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Grille(Flotte flotte) {
		// ///////////////////////////////////////////////setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 453, 318);
		contentPane = this;// ///////////////////new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new LineBorder(Color.RED));
		// ///////////////////////////////////////////////setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(10, 10, 0, 0));

		BlocVisu anc = null;
		// -----mise en place des zones
		for (int i = 0; i < 100; i++) {
			// ---création d'un bloc Visuel
			BlocVisu bvisu = new BlocVisu(i, -1, this);
			Bloc b = bvisu.getBloc();
			bvisu.setBground(Color.lightGray);

			// ----est-ce qu'il correspond à un des blocs d'un des batiments de
			// la flotte ?
			Batiment bat = flotte.getBatiment(b.getX(), b.getY());
			if (bat != null) { // ----un batiment existe à cette position
				// ----à cette position existe-t-il un bloc ?
				if (bat.getBloc(b.getX(), b.getY()) != null) {
					bvisu.setEtat(1);
					bvisu.setVisible(false);
				}
			} else
				bvisu.setVisible(false);

			bvisu.setBorder(new LineBorder(Color.cyan));
			contentPane.add(bvisu);
			anc = bvisu;
		}

		// MouseAdapter u = new MouseAdapter();
		// u.mouseMoved(e);
		this.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseMoved(MouseEvent e) {
				System.out.println(e.getX());
			}

		});

	}

	// public List<Bloc> getBlocs(int nbBloc, ) {
	// return bloc;
	// }
	public void afficheFlotte() {
		// -----
		for (int i = 0; i < 100; i++) {
			BlocVisu bvisu = new BlocVisu(i, 0, this);
			Bloc b = bvisu.getBloc();
			Batiment bat = flotte.getBatiment(b.getX(), b.getY());
			if (bat != null) {
				bvisu.setEtat(1);
				bvisu.setVisible(true);
			} else
				bvisu.setVisible(false);
		}
	}

	public List<BlocVisu> getBlocs(String VH, int x, int y, int nbBloc) {
		List<BlocVisu> ret = new ArrayList<BlocVisu>();
		while (nbBloc > 0) {
			BlocVisu b = null;
			int pos = 0;
			try {
				if (VH == "V")
					pos = (y - nbBloc) * 10 + x;
				else
					// Horizontal
					pos = (y) * 10 + (x - (nbBloc));
				
				if (pos>=0)
					b = (BlocVisu) contentPane.getComponent(pos);
				
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
			ret.add(b);
			nbBloc--;
		}
		return ret;
	}

	public BlocVisu getBloc(String VH, int x, int y) {
		BlocVisu b = null;
		b = (BlocVisu) contentPane.getComponent((y) * 10 + (x - 1));

		return b;
	}

	public int getX(int pos) {
		// TODO Auto-generated method stub
		return pos - (getY(pos) * 10);
	}

	public int getY(int pos) {
		// TODO Auto-generated method stub
		return pos / 10;
	}
}
