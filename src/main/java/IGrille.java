

import java.util.List;

public interface IGrille {
	
	List<BlocVisu> getBlocs( String VH, int x, int y, int nbBloc);
	
	BlocVisu getBloc( String VH, int x, int y);
	
	int getX(int pos);
	int getY(int pos);
	String getDir();
	void setDir(String dir);

}
