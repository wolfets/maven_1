

import java.util.ArrayList;
import java.util.List;

/**
 * @author HumanBooster
 *
 */

public class Batiment {
	private String Titre;
	private List<Bloc> blocs;
	
	
			
	public String getTitre() {
		return Titre;
	}
	public void setTitre(String titre) {
		Titre = titre;
	}
	public List<Bloc> getBlocs() {
		return blocs;
	}
	public void setBlocs(List<Bloc> blocs) {
		this.blocs = blocs;
	}
	public Bloc getBloc(int x, int y) {
		for (Bloc b : blocs) {
			if (b.getX()==x && b.getY()==y)
				return b;
		}
		return null;
	}

	public Batiment() {
		blocs = new ArrayList<Bloc>();
	}
	
	public Batiment(List<Bloc> blocs) {
		blocs = new ArrayList<Bloc>();
		this.blocs = blocs;
		
	}
	public Batiment(String c, int x, int y, int l) {
		if(blocs==null){
			blocs = new ArrayList<Bloc>();
		}
		if(c=="V"){
			for(int i=0; i<l;i++){
				Bloc b = new Bloc(x, y + i);
				blocs.add(b);
			}
			
		}else if (c=="H"){
			for(int i=0; i<l;i++){
				Bloc b = new Bloc(x + i, y);
				blocs.add(b);
			}
		}
	}
	public List<Bloc> addBloc(Bloc b){
		blocs.add(b);
		return blocs;
	}

	/**
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isBlocTouche(int x, int y){
		boolean r = false;
		for (Bloc b : blocs) {
			if(b.getX()==x && b.getY()==y){
				r=true;
				b.setEtat(0);
			}
		}
		return r;
	}
	public boolean isCoule() {
		return (nbBlocEncoreValid()==0);
	}
	
	public int nbBlocEncoreValid(){
		int totBlocValid = 0;
		for (Bloc b : blocs) {
			totBlocValid += b.getEtat();
		}
		
		return totBlocValid;
		
	}
	
}
