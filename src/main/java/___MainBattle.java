

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;

//import net.miginfocom.swing.MigLayout;

import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observer;

import javax.swing.border.BevelBorder;

public class ___MainBattle {

	private JFrame frame;
	private Grille g;
	private Grille g2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					___MainBattle window = new ___MainBattle();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ___MainBattle() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Flotte flotte1 = new Flotte();
		Flotte flotte2 = new Flotte();

		flotte1.createBatiment("V", 1, 2, 3);
		flotte1.createBatiment("H", 5, 3, 2);

		flotte2.createBatiment("V", 1, 2, 3);

		frame.getContentPane().setLayout(new GridLayout(0, 3));

		g = new Grille(flotte1);
		g.setBackground(Color.PINK);
		g.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		this.frame.getContentPane().add(g);// getContentPane().add(g_1);

		JPanel milieu = new JPanel();
		milieu.setBackground(Color.GREEN);

		Joueur joueur1 = new Joueur("toto", flotte1);
		JPanel milieu1 = new Panneau(joueur1);
		milieu1.setBackground(Color.GREEN);
		milieu.add(milieu1);

		Joueur joueur2 = new Joueur("titi", flotte2);
		JPanel panel = new Panneau(joueur2);
		panel.setBackground(new Color(204, 255, 255));
		milieu.add(panel);

		// milieu.con getContentPane().setLayout(new GridLayout(0, 3));

		this.frame.getContentPane().add(milieu);// getContentPane().add(g_1);
		milieu.setLayout(new GridLayout(2, 0, 0, 0));
		g2 = new Grille(flotte2);
		this.frame.getContentPane().add(g2);// getContentPane().add(g_1);

		// frame.getChildren().add(g);
		// frame.add(g);
/*
		this.frame.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				System.out.println("tester");
			}

			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println("2test2");
			}

			@Override
			public void keyTyped(KeyEvent e) {
				System.out.println("3test3");
			}
		});*/
		this.frame.requestFocus();

		ObservableValue ov = new ObservableValue(0);
		TextObserver to = new TextObserver(ov);
		ov.addObserver(to);
		
		ov.setValue(10);		
		ov.setValue(20);		
		ov.setValue(40);

	}
}
