

import java.util.Observable;

public class ObservableValue extends Observable {
	private int n = 0;
	private String nom;

	public ObservableValue(int n) {
		this.n = n;
	}

	/**
	 * @param n
	 */
	public void setValue(int n) {
		this.n = n;
		
		setChanged();
		notifyObservers();
	}

	public int getValue() {
		return n;
	}
	/**
	 * @param n
	 */
	public void setNom(String n) {
		this.nom = n;
		
		setChanged();
		notifyObservers();
	}

	public String getNom() {
		return nom;
	}
}
